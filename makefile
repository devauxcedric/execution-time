CC=gcc
CFLAGS= -std=c11 -Wall -Wextra -pedantic
DEPS = config.h colors.h result.h timer.h graphic.h prog_launcher.h
OBJ = config.c result.c timer.c graphic.c prog_launcher.c

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

timer: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)
	rm -f *.o

clean:
	rm timer
	rm -f *.o
