#ifndef PROGLAUNCHER_H
#define PROGLAUNCHER_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

void launch_programm(const char *program, const char *argv[]);

#endif
