#include "prog_launcher.h"

void handle_exit(pid_t cid, int status) {
  printf("Return status = %d\n", WEXITSTATUS(status));

  if (WIFSIGNALED(status)) {
    printf("child %d process was terminated by a signal.\n", cid);
    if (WTERMSIG(status)) {
      printf("child %d signal %d caused the child process to terminate.\n", cid,
             WTERMSIG(status));
    }
    exit(1);
  }

  if (WEXITSTATUS(status) != 0) {
    printf("Child %d returned an error status not equal to zero: %d. This mean "
           "that "
           "the execv failed or that the executed programm ended with a non "
           "zero exit status which is considered as an anormal execution.\n",
           cid, WEXITSTATUS(status));
    exit(1);
  }
}

void launch_programm(const char *program, const char *argv[]) {
  int forked = fork();
  int exit_status;

  if (forked == 0) {
    execvp((char *)program, (char *const *)argv);

    // If this line launches it means the execv failed
    perror("Execv: error while trying to launch the program");
    exit(-1);

  } else if (forked > 0) {
    waitpid(forked, &exit_status, 0);
    handle_exit(forked, exit_status);

  } else {
    printf("Fork error\n");
    exit(-1);
  }
}
