#ifndef RESULT_H
#define RESULT_H

#include "colors.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

typedef struct results {
  time_t res_sec;
  clock_t res_mili;
  struct results *next;
} results;

results *create_res(long res_sec, long res_tick, results *old);
void show_result(results *res);
void free_all_res(results *res);
results *append_result_end(results *oldm, results *newm);
void print_res_file(results *res);

#endif
