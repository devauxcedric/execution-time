#include "graphic.h"

char* printer_args(results *res, int start, int end) {
	results *act = res;
	char *result = calloc((end - start) * 2 + 2, sizeof(char) * 32);
	char *tmp = malloc(sizeof(char) * 32);

	strcat(result, "./printer.py ");

	for (int i = start; i <= end; i++) {
		snprintf(tmp, 32, "%d", i);
		strcat(result, tmp);
		if (i <= end - 1) {
			strcat(result, ",");
		}
	}
	strcat(result, "#");

	while (act != NULL) {
		snprintf(tmp, 32, "%ld.%06ld", act->res_sec, act->res_mili);
		strcat(result, tmp);
		if (act->next != NULL) {
			strcat(result, ",");
		}
		act = act->next;
	}
	free(tmp);
	printf("Result %s\n", result);
	return result;
}

void output_data_to_gnuplot_file(results *res, int start) {
	FILE *output_file = fopen(".data", "w");
	results *act = res;
	int act_counter = start;

	while (act != NULL) {
		fprintf(output_file,"%d %ld.%06ld\n",act_counter,act->res_sec,act->res_mili);
		act_counter++;
		act = act->next;
	}
	fclose(output_file);
}

void graphic_results(results *res, int start, int end) {
	printf("Launching python script\n");
	output_data_to_gnuplot_file(res, start);
	system("gnuplot -persist -c gnuplot_script.txt");
}
