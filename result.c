#include "config.h"
#include "result.h"

// Create a new result linked-list (initiated with one element) for time in
// second and tick.
results *create_res(long res_sec, long res_mili, results *old) {
  results *new = malloc(sizeof(struct results));
  new->res_sec = res_sec;
  new->res_mili = res_mili;
  new->next = NULL;

  if (old != NULL) {
    return append_result_end(old, new);
  }

  return new;
}

// Append the result (new_res) at the end of the link (res_list)
results *append_result_end(results *res_list, results *new_res) {
  if (res_list == NULL) {
    return new_res;
  }

  results *act = res_list;
  while (act->next != NULL) {
    act = act->next;
  }
  act->next = new_res;
  return res_list;
}

// Iterate on the list and free all the links
void free_all_res(results *res_list) {
  results *act = res_list;
  results *save = res_list;
  while (act != NULL) {
    save = act->next;
    free(act);
    act = save;
  }
} // test

// A simple iteration to show the content of each link of the list
void show_result(results *res_list) {
  if (res_list == NULL) {
    printf("No result to be shown\n");
    return;
  }
  printf("Showing all results :\n\n");
  results *act = res_list;
  while (act != NULL) {
    printf("Time in sec : " RED "%ld" RESET "\n"
           "with : " GRN "%06ld" RESET " miliseconds\n",
           act->res_sec, act->res_mili);
    act = act->next;
  }
  printf("\n");
}

// Generate the new filename for stocking the results
char *create_res_file_name() {
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  char *date = malloc(sizeof(char) * strlen(get_res_directory()) + 20);
  // The name contains the res directory wich is a global_variable and is
  // constitued of the actual date and hour
  sprintf(date, "%s/%d_%d_%d_%d_%d_%d", get_res_directory(), tm.tm_year + 1900,
          tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
  return date;
}

// Write the results contained in the res_list to the given file
void write_result_file(FILE *file, results *res_list) {
  results *act = res_list;
  int count = 0;
  while (act != NULL) {
    fprintf(file, "%d : %lds/%ldt\n", count, act->res_sec, act->res_mili);
    count++;
    act = act->next;
  }
}

// Create the result dir if it don't exist
void create_res_dir() {
  struct stat st = {0};

  if (stat(get_res_directory(), &st) == -1) {
    mkdir(get_res_directory(), 0777);
  }
}

// Print the results in a file for later use by the programm user
void print_res_file(results *res_list) {
  if (res_list == NULL)
    return;
  char *filename = create_res_file_name();
  printf("Filename = %s\n", filename);
  create_res_dir();

  FILE *file = fopen(filename, "w");
  assert(file != NULL);

  write_result_file(file, res_list);

  free(filename);
  free(get_res_directory());
  fclose(file);
}
