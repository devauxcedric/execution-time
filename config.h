#ifndef CONFIG_H
#define CONFIG_H

#include <stdio.h>
#include <stdlib.h>

void set_res_directory(const char *name);
char* get_res_directory();
void read_directory();

#endif
