#define _DEFAULT_SOURCE
#ifndef TIMER_H
#define TIMER_H

#include "config.h"
#include "graphic.h"
#include "prog_launcher.h"
#include "result.h"
#include <math.h>
#include <stdarg.h>
#include <stdbool.h>
#include <sys/time.h>

void loop_one(int loops);
int calc_av(long *start, long *end, int precision);
void free_all(int num, ...);
results *execute_fonction(int precision, const char *program, const char **argv,
                          int argc, results *res);

#endif
