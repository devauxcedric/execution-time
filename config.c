#include "config.h"

char *RES_DIRECTORY;


// Modify the .config file to write the given directory name
void set_res_directory(const char *name) {
  FILE *config = fopen(".config\0", "w+");
  fprintf(config, "%s\n", name);
  fclose(config);
}

char* get_res_directory(){
	return RES_DIRECTORY;
}


// Give the size of the string corresponding to the absolute path to the
// directory
int find_dir_size(FILE *file) {
  int counter = 0;
  while (fgetc(file) != '\n') {
    counter++;
  }
  return counter;
}

// Modify the global variable res_dir used by the programm to know where to
// wirte the result files
void read_directory() {
  FILE *config = fopen(".config", "r");
  // If no .config exist we create one
  if (config == NULL) {
    printf("No previous directory used choosing \"res_dir\"\n");
    set_res_directory("res_dir");
    config = fopen(".config", "r");
    if (config == NULL) {
      printf("Error while opening the result directory\n");
      exit(1);
    }
  }

  int dir_size = find_dir_size(config);
  rewind(config);
  RES_DIRECTORY = malloc(sizeof(char) * dir_size);
  fread(RES_DIRECTORY, sizeof(char), dir_size, config);
  fclose(config);
}
