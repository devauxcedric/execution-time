#include "timer.h"

int arguments_start = 2;
bool dir_selected = false;
bool interval_mode = false;
int start_inter = 0;
int end_inter = 0;
bool command_mode = false;

// Calculate the average time (end array - start array)
int calc_av(long *start, long *end, int arrays_size) {
  long average = 0;
  for (int i = 0; i < arrays_size; i++) {
    average += (end[i] - start[i]);
  }
  average = average / arrays_size;
  return average;
}

struct timeval calc_av_time(struct timeval *start, struct timeval *end,
                            int time_nb) {
  struct timeval average;
  timerclear(&average);
  double dif = 0.0;

  for (int time_index = 0; time_index < time_nb; time_index++) {
    struct timeval difference;
    timersub(&end[time_index], &start[time_index], &difference);

    dif += difference.tv_sec / (double)time_nb;
    dif += (difference.tv_usec / (double)time_nb) / 1000000.0;
  }

  average.tv_usec = (long)(modf(dif, &dif) * 1000000);
  average.tv_sec = (int)dif;

  return average;
}

// Free all the (num) linked list given to it
void free_all(int num, ...) {
  // Va_list is an iterator of size num
  va_list list;
  va_start(list, num);
  for (int i = 0; i < num; i++)
    // argument type must be a long for the time
    free(va_arg(list, long *));
  va_end(list);
}

char *prepare_command_arg(const char *program, const char **argv, int argc) {
  int allocated = 0;
  char *arguments;
  for (int i = 0; i < argc; i++) {
    allocated += strlen(argv[i]) + 1;
    arguments = realloc(arguments, allocated * sizeof(char));
    for (size_t j = 0; j < strlen(argv[i]); j++) {
      arguments[allocated - 1 - strlen(argv[i] + j)] = argv[i][j];
    }
    arguments[allocated - 1] = ' ';
  }
  arguments[allocated] = (char)0;
  return arguments;
}

// Execute a given function, calculate the average time in second and tick and
// return the result
results *execute_fonction(int precision, const char *program, const char **argv,
                          int argc, results *res) {
  struct timeval *start_time = malloc(sizeof(struct timeval) * precision);
  struct timeval *end_time = malloc(sizeof(struct timeval) * precision);

  for (int i = 0; i < precision; i++) {
    struct timeval timer;
    gettimeofday(&timer, NULL);
    start_time[i] = timer;

    // Execution the programm to test
    if (command_mode) {
      system(prepare_command_arg(program, argv, argc));
    } else {
      launch_programm(program, argv);
    }

    gettimeofday(&timer, NULL);
    end_time[i] = timer;
  }

  // Free the 4 arrays used to stock the results
  struct timeval average = calc_av_time(start_time, end_time, precision);
  free_all(2, start_time, end_time);

  // return create_res(average_sec, average_mili, res);
  return create_res(average.tv_sec, average.tv_usec, res);
}

int verify_precision(const char *precision) {
  if (atoi(precision) <= 0) {
    printf("Precision can't less or equal to 0. Set to 1.\n");
    return 1;
  }
  return atoi(precision);
}

void print_usage() {
  printf("Usage : ./timer precision [-r res_directory] programm [arguments]\n");
  exit(0);
}

const char **prepare_arguments(int interval_act, const char **argv, int argc) {
  char *buffer = malloc(sizeof(char) * sizeof(int) * 8);
  snprintf(buffer, sizeof(int) * 8, "%d", interval_act);

  char **new_arguments =
      (char **)malloc((argc - arguments_start + 1) * sizeof(char *));

  size_t sz;
  for (int i = arguments_start, placed = 0; i < argc; i++, placed++) {
    sz = strlen(argv[i]);
    new_arguments[placed] = malloc(sz * sizeof(char));
    strcpy(new_arguments[placed], argv[i]);
  }

  sz = strlen(buffer) + 1;
  new_arguments[argc - arguments_start] = malloc(sz * sizeof(char));
  strcpy(new_arguments[argc - arguments_start], buffer);
  new_arguments[argc - arguments_start + 1] = (char *)0;

  return (const char **)new_arguments;
}

void verify_interval() {
  if (end_inter <= start_inter) {
    printf("You selected an invalid interval: start_inter= %d, end_inter= "
           "%d\n",
           start_inter, end_inter);
    exit(-1);
  }
}

void parse_arguments(int argc, char const *argv[]) {
  if (argc < 3) {
    print_usage();
    exit(1);
  }
  for (int arg_act = 2; arg_act < argc; arg_act++) {
    // If the use specify a directory to write the results
    printf("Reading %s\n", argv[arg_act]);
    if (strcmp(argv[arg_act], "-r") == 0) {
      set_res_directory(argv[arg_act + 1]);
      dir_selected = true;
      arguments_start += 2;
      // Change the default directory
    } else if (strcmp(argv[arg_act], "-d") == 0) {
      set_res_directory(argv[arg_act + 1]);
      printf("Changed default directory to %s\nExiting\n", argv[arg_act + 1]);
      exit(0);
    } else if (strcmp(argv[arg_act], "-c") == 0) {
      command_mode = true;
      arguments_start += 1;
    } else if (strcmp(argv[arguments_start], "-i") == 0) {
      interval_mode = true;
      start_inter = atoi(argv[arguments_start + 1]);
      end_inter = atoi(argv[arguments_start + 2]);
      verify_interval();
      arguments_start += 3;
    }
  }
}

void prepare_res_directory() {
  read_directory();
  if (!dir_selected) {
    printf("Warning last directory selected : \n%s\n Are you sure you want to "
           "continue ?[Y/n]:\n",
           get_res_directory());
    char want_continue = getchar();
    if (want_continue != 'Y' && want_continue != 'y') {
      printf("Program exited by user choice\n");
      exit(1);
    }
  }
}

int main(int argc, char const *argv[]) {

  // Parsing arguments
  int precision = verify_precision(argv[1]);

  parse_arguments(argc, argv);

  prepare_res_directory();
  printf("Arguments start = %d\n", arguments_start);

  /*All the functions to execute*/
  results *res = NULL;
  if (interval_mode) {
    for (int interval_act = start_inter; interval_act <= end_inter;
         interval_act++) {
      const char **new_arguments = prepare_arguments(interval_act, argv, argc);
      res = execute_fonction(precision, argv[arguments_start], new_arguments,
                             argc - arguments_start, res);
      free(new_arguments);
    }
  } else {
    res = execute_fonction(precision, argv[arguments_start],
                           argv + arguments_start, argc - arguments_start, res);
  }

  show_result(res);
  print_res_file(res);
  if (interval_mode) {
    graphic_results(res, start_inter, end_inter);
  }
  free_all_res(res);
}
